import React, {useEffect, useState} from 'react';
import Selector from "./Selector";

export default function CarsSelector(props)
{
    let parser = (data) => {
        return data.map((option) => {
            return {name: option.plate, value: option.id}
        });
    }

    return <Selector
        itemID={props.itemID}
        name={props.name}
        placeholder={props.placeholder}
        onChange={props.onChange}
        path="api/travels/cars"
        parseOptions={parser}
    />
}
