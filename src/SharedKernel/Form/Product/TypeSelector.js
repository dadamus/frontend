import React, {useEffect, useState} from "react";
import {get} from "../../Api";
import Selector from "../Selector";

export default function TypeSelector(props) {
    let parser = (data) => {
        return data.map((option) => {
            return {name: option.name, value: option.id}
        });
    }

    return <Selector
        itemID={props.itemID}
        name={props.name}
        placeholder={props.placeholder}
        onChange={props.onChange}
        path="api/product/types"
        parseOptions={parser}
    />
}
