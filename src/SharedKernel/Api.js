import React from 'react';

const apiUrl = 'http://127.0.0.1/'

export function get(path) {
    const token = sessionStorage.getItem('token');

    return fetch(
        apiUrl + path,
        {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }
    ).then((response) => {
        if (response.status === 401) {
            sessionStorage.removeItem('token');
            window.location.href = "/";
        }

        return response;
    });
}

export function post(path, data) {
    const token = sessionStorage.getItem('token');

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    };

    return fetch(
        apiUrl + path,
        {
            method: 'POST',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data)
        }
    ).then((response) => {
        if (response.status === 401) {
            sessionStorage.removeItem('token');
            window.location.href = "/";
        }

        return response;
    });
}

export function postUnauthorized(path, data) {
    let headers = {
        'Content-Type': 'application/json'
    };

    return fetch(
        apiUrl + path,
        {
            method: 'POST',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data)
        }
    )
}
