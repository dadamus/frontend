import React, {useEffect, useState} from 'react';
import {
    useParams
} from "react-router-dom";
import {ButtonGroup, Col, Row} from "react-bootstrap";
import NewProductModal from "./ProductList/NewProductModal";
import Filters from "./ProductCard/Filters";
import DataTable from "../SharedKernel/DataTable";
import GenerateCodes from "./ProductCard/GenerateCodes";
import AddCorrection from "./ProductCard/AddCorrection";
import ProductCardButton from "./ProductList/ProductCardButton";
import {post} from "../SharedKernel/Api";
import {toast} from "react-toastify";

export default function ProductCard() {
    let {id} = useParams();

    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'code_id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },

        {
            data: 'type_name',
            label: 'Typ',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'device_name',
            label: 'Nazwa urządzenia',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'date',
            label: 'Data',
            searchable: true,
            search: {
                value: null
            }
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            post(
                'api/product/card/' + id + '/state', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data)
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Produkty
                <div className="breadcrumb">
                    Lista produktów
                </div>
                <div className="breadcrumb">
                    Karta produktu
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Karta produktu
                        </div>
                        <div className="toolbar">
                            <ButtonGroup>
                                <AddCorrection productId={id}/>
                                <GenerateCodes productId={id}/>
                            </ButtonGroup>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters changeFilters={changeFilters} search={search}/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
