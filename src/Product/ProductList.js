import React, {useEffect, useState} from 'react';
import {Button, Col, Container, Row} from "react-bootstrap";
import Filters from "./ProductList/Filters";
import DataTable from "../SharedKernel/DataTable";
import NewProductModal from "./ProductList/NewProductModal";
import {post} from "../SharedKernel/Api";
import {toast} from 'react-toastify';
import Actions from "./ProductList/Actions";
import ProductCardButton from "./ProductList/ProductCardButton";

export default function ProductList() {
    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null
            },
            render: ProductCardButton
        },
        {
            data: 'position_name',
            label: 'Pozycja',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'category_name',
            label: 'Kategoria',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'type_name',
            label: 'Typ',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'action',
            label: 'Akcje',
            searchable: false,
            search: {
                value: null
            },
            width: '10%',
            render: Actions
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            post(
                'api/products/list/filter', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data)
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Produkty
                <div className="breadcrumb">
                    Lista produktów
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista produktów
                        </div>
                        <div className="toolbar">
                            <NewProductModal/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters changeFilters={changeFilters} search={search}/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
