import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";

export default function ProductCardButton(props) {
    return <Link to={"/product/card/" + props.row.id}>{props.row.name}</Link>
}
