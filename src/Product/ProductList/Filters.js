import React, {useState} from "react";
import {Button, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faTimes} from "@fortawesome/free-solid-svg-icons";
import ProductTypeSelector from "../../SharedKernel/Form/Product/TypeSelector";
import ProductCategorySelector from "../../SharedKernel/Form/Product/CategorySelector";

export default function Filters(props) {
    return <div className="filters">
        <form>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="name">Nazwa</label>
                        <input
                            type="text"
                            className="form-control form-control-sm"
                            onChange={event => props.changeFilters('name', event.target.value)}
                            itemID="name"
                            name="name"
                            placeholder="Nazwa"
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Typ</label>
                        <ProductTypeSelector
                            itemID="type"
                            name="type"
                            placeholder="Wybierz"
                            onChange={event => props.changeFilters('type', event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Kategoria</label>
                        <ProductCategorySelector
                            itemID="category"
                            name="category"
                            placeholder="Wybierz"
                            onChange={event => props.changeFilters('category', event.target.value)}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <Button onClick={props.search}><FontAwesomeIcon icon={faSearch}/> Szukaj</Button>
                    &nbsp;&nbsp;
                    <Button variant="secondary">
                        <FontAwesomeIcon icon={faTimes}/> Reset
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
        </form>
    </div>;
}
