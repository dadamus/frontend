import React, {useState} from 'react';
import {Button, Col, Container, Modal, Row, Table} from "react-bootstrap";
import NewCarModal from "../../Settings/Cars/NewCarModal";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import ProductTypeSelector from "../../SharedKernel/Form/Product/TypeSelector";
import ProductCategorySelector from "../../SharedKernel/Form/Product/CategorySelector";
import ProductPositionsSelector from "../../SharedKernel/Form/Product/PositionsSelector";

export default function NewProductModal() {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return <>
        <Button onClick={handleShow}><FontAwesomeIcon icon={faPlus}/> Dodaj produkt</Button>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Formularz auta</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Nazwa</label>
                        <input type="text" className="form-control" itemID="name" name="name" placeholder="Nazwa"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Opis</label>
                        <textarea className="form-control" itemID="description" name="description"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="position">Stanowisko</label>
                        <ProductPositionsSelector itemID="position" name="position"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="position">Kategoria</label>
                        <ProductCategorySelector itemID="category" name="category"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="position">Typ</label>
                        <ProductTypeSelector itemID="type" name="type"/>
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Zamknij
                </Button>
                <Button variant="primary" onClick={handleClose}>
                    Dodaj
                </Button>
            </Modal.Footer>
        </Modal>
    </>;
}
