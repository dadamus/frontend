import React, {useEffect, useState} from 'react';
import {Col, Container, Row, Table} from "react-bootstrap";
import NewCarModal from "./Cars/NewCarModal";
import {get} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import DataTable from "../SharedKernel/DataTable";

export default function Cars() {
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'plate',
            label: 'Nr Rej',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'visible',
            label: 'Aktywny',
            searchable: true,
            search: {
                value: null
            },
            render: (column) => column.row.visible === 1 ? "Tak" : "Nie"
        }
    ];

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            get(
                'api/settings/cars'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data)
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Ustawienia
                <div className="breadcrumb">
                    Auta
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista aut
                        </div>
                        <div className="toolbar">
                            <NewCarModal refreshList={search}/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <DataTable
                            columns={columns}
                            rows={rows}
                            loading={loading}
                        />
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
