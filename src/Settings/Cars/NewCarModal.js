import React, {useState} from 'react';
import {Button, FormCheck, Modal} from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function NewCarModal(props) {
    const [loading, setLoading] = useState(false);
    const [show, setShow] = useState(false);
    const [plate, setPlate] = useState('');

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    let handleSubmit = () => {
        setLoading(true);

        post(
            'api/settings/car/add',
            {plate}
        ).then((response) => {
            if (response.status !== 200) {
                throw 'Wystapil blad!';
            }

            return response.json();
        }).then((data) => {
            toast.success('Nowy pojazd został dodany!');
            props.refreshList();
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setLoading(false);
            handleClose();
        })
    };

    return <>
        <Button onClick={handleShow}><FontAwesomeIcon icon={faPlus}/> Dodaj auto</Button>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Formularz auta</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Numer rejestracyjny</label>
                        <input
                            type="text"
                            className="form-control"
                            itemID="name"
                            name="name"
                            placeholder="Nazwa"
                            value={plate}
                            onChange={event => setPlate(event.target.value)}
                        />
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose} disabled={loading}>
                    Zamknij
                </Button>
                <Button variant="primary" onClick={handleSubmit} disabled={loading}>
                    Dodaj
                </Button>
            </Modal.Footer>
        </Modal>
    </>;
}
