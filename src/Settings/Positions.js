import React, {useEffect, useState} from 'react';
import {Col, Container, Row, Table} from "react-bootstrap";
import NewPositionModal from "./Positions/NewPositionModal";
import DataTable from "../SharedKernel/DataTable";
import {get} from "../SharedKernel/Api";
import {toast} from "react-toastify";

export default function Positions() {
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'description',
            label: 'Opis',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'visible',
            label: 'Aktywny',
            searchable: true,
            search: {
                value: null
            },
            render: (column) => column.row.visible === 1 ? "Tak" : "Nie"
        }
    ];

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            get(
                'api/settings/positions'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data)
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Ustawienia
                <div className="breadcrumb">
                    Stanowiska
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista stanowisk
                        </div>
                        <div className="toolbar">
                            <NewPositionModal refreshList={search}/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <DataTable
                            columns={columns}
                            rows={rows}
                            loading={loading}
                        />
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
