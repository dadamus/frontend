import React from 'react';
import {Col, Container, Row} from "react-bootstrap";

export default function () {
    return <Container>
        <Row className="subhead">
            <div className="title">
                Dashboard
                <div className="breadcrumb">
                    Dashboard
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={8}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Urządzenia
                        </div>
                    </div>
                    <div className="portlet-body">
                        block
                    </div>
                </div>
            </Col>
            <Col lg={4}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Aktywne urzadzenia
                        </div>
                    </div>
                    <div className="portlet-body">
                        block
                    </div>
                </div>

                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Ostatnio zaczytane kody
                        </div>
                    </div>
                    <div className="portlet-body">
                        block
                    </div>
                </div>
            </Col>
        </Row>
    </Container>;
}
