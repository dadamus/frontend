import React, {useState} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faTimes} from "@fortawesome/free-solid-svg-icons";
import FromToDatePicker from "../../SharedKernel/Form/FromToDatePicker";

export default function Filters() {
    return <div className="filters">
        <form>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Data produkcji:</label>
                        <FromToDatePicker/>
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Data trasy:</label>
                        <FromToDatePicker/>
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Produkt:</label>
                        <select className="form-control form-control-sm" itemID="category" name="product" placeholder="Wyszukaj produkt">

                        </select>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Stanowisko produkcji:</label>
                        <select className="form-control form-control-sm" itemID="category" name="product" placeholder="Wybierz">

                        </select>
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Stanowisko załadunku:</label>
                        <select className="form-control form-control-sm" itemID="category" name="product" placeholder="Wybierz">

                        </select>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <Button><FontAwesomeIcon icon={faSearch}/> Szukaj</Button>
                    &nbsp;&nbsp;
                    <Button variant="secondary">
                        <FontAwesomeIcon icon={faTimes}/> Reset
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
        </form>
    </div>;

}
