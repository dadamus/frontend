import React, {useState} from 'react';
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {postUnauthorized} from "../SharedKernel/Api";
import { toast } from 'react-toastify';

import "./LoginPage.css";

export default function LoginPage(props) {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [loginDisabled, setLoginDisabled] = useState(false);

    const loginAction = () => {
        setLoginDisabled(true);

        postUnauthorized(
            'api/login',
            {
                username: login,
                password: password
            }
        ).then(response => {
            if (response.status === 401) {
                throw new TypeError("Błędny login lub hasło!");
            }

            return response.json();
        }).then((data) => {
            sessionStorage.setItem('token', data.token);
            window.location.href = "/";
            toast.success('Logowanie udane!');
        }).catch((reason) => {
            toast.error('Błędny login lub hasło!');
        }).finally(() => {
            setLoginDisabled(false);
        });
    };

    return <Container className="main-container login-container" fluid>
        <Row className="justify-content-md-center">
            <Col lg={4} className="login-title">
                Wymagana autoryzacja<br/><br/>
            </Col>
        </Row>
        <Row className="justify-content-md-center">
            <Col lg={4}>
                <Form>
                    <Form.Group>
                        <Form.Control type="text" disabled={loginDisabled} value={login} placeholder="Login"
                                      onChange={event => setLogin(event.target.value)}/>
                        <br/>
                        <Form.Control type="password" disabled={loginDisabled} value={password} placeholder="Hasło"
                                      onChange={event => setPassword(event.target.value)}/>
                        <br/>
                        <Button variant="primary" disabled={loginDisabled} onClick={loginAction}>
                            Zaloguj się
                        </Button>
                    </Form.Group>
                </Form>
            </Col>
        </Row>
    </Container>;
}
