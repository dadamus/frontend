import React, {useState} from 'react';
import {Button, Modal} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import DatePicker from "react-datepicker";
import CarsSelector from "../../SharedKernel/Form/CarsSelector";
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function AddTravel(props)
{
    const [loading, setLoading] = useState(false);
    const [show, setShow] = useState(false);
    const [travelData, setTravelData] = useState({});

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleSubmit = () => {
        setLoading(true);

        post(
            'api/travel',
            travelData
        ).then((response) => {
            if (response.status !== 200) {
                throw 'Wystapil blad!';
            }

            return response.json();
        }).then(() => {
            toast.success('Trasa została dodana!');
            setTravelData({});
            props.refreshRows();
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setLoading(false);
            handleClose();
        })
    };

    let changeFormData = (name, value) => {
        let newData = {};
        newData[name] = value;

        setTravelData({...travelData, ...newData})
    }

    return <>
        <Button onClick={handleShow}><FontAwesomeIcon icon={faPlus}/> Dodaj trase</Button>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Formularz trasy</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Nazwa</label>
                        <input type="text"
                               className="form-control"
                               value={travelData.name}
                               itemID="name"
                               name="name"
                               placeholder="Nazwa"
                               onChange={event => changeFormData('name', event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Opis</label>
                        <textarea
                            className="form-control"
                            itemID="description"
                            name="description"
                            value={travelData.description}
                            onChange={event => changeFormData('description', event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Data</label><br/>
                        <DatePicker
                            dateFormat="dd-MM-yyyy"
                            className="form-control col"
                            placeholderText="Data"
                            selected={travelData.date}
                            onChange={date => changeFormData('date', date)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Numer rejestracyjny</label>
                        <CarsSelector
                            itemID="car_id"
                            name="car_id"
                            placeholder="Wybierz..."
                            onChange={event => changeFormData('car_id', event.target.value)}/>
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose} disabled={loading}>
                    Zamknij
                </Button>
                <Button variant="primary" onClick={handleSubmit} disabled={loading}>
                    Dodaj
                </Button>
            </Modal.Footer>
        </Modal>
    </>;
}
