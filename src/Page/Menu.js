import React, {useState} from 'react';
import {Container, Nav, NavDropdown, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Menu() {
    const [active_key, setActiveKey] = useState("2");

    return <Container fluid className="menu">
        <Row>
            <Nav variant="pills" activeKey={active_key} onSelect={setActiveKey}>
                <Nav.Item>
                    <Link to="/" className="nav-link">Dashboard</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/products/list" className="nav-link">Produkty</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/code/list" className="nav-link">Kody</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/travel/list" className="nav-link">Trasy</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/" className="nav-link">Raporty</Link>
                </Nav.Item>
                <NavDropdown id="settings-dropdown" title="Ustawienia">
                    <Link to="/settings/devices" className="dropdown-item">
                        Urządzenia
                    </Link>
                    <Link to="/settings/positions" className="dropdown-item">
                        Stanowiska
                    </Link>
                    <Link to="/settings/cars" className="dropdown-item">
                        Auta
                    </Link>
                </NavDropdown>
            </Nav>
        </Row>
    </Container>;
}
